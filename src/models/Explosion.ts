'use strict'

export class Explosion {
  leftPos:number = -100
  topPos :number = -100
  type:string
  lifespan:number=0
  size:number = 40

  constructor (leftPos:number, topPos :number, type:string, size:number) {
    this.leftPos = leftPos
    this.topPos  = topPos
    this.type = type
    this.size = size
  }
}
