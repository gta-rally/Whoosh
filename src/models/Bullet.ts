'use strict'

export class Bullet {
  type:string=''
  leftPos:number = -100
  topPos :number = -100
  direction: number = 0
  speed: number = 0
  lifespan: number = 0
  counter:number = 0
  width:number
  height:number

  constructor (type:string,leftPos:number, topPos :number, direction:number,speed:number,lifespan:number, width:number, height:number) {
    this.type = type
    this.leftPos = leftPos
    this.topPos  = topPos
    this.direction = direction
    this.speed = speed
    this.lifespan = lifespan
    this.width = width
    this.height=height
  }
}
