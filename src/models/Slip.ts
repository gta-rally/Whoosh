'use strict'

export class Slip {
  leftPos:number = -100
  topPos :number = -100
  direction:number

  constructor (leftPos:number, topPos :number, direction:number) {
    this.leftPos = leftPos
    this.topPos  = topPos
    this.direction = direction
  }
}
