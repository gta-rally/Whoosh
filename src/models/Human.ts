'use strict'

export class Human {
  leftPos:number = -100
  topPos :number = -100
  alive:boolean=true
  moveDirection:number = 180
  movementSpeed:number=1

  constructor (leftPos:number, topPos :number) {
    this.leftPos = leftPos
    this.topPos  = topPos
  }
}
