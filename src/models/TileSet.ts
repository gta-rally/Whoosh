'use strict'
import tileData from '../assets/tileData.json';
import { Tile } from './Tile';
import { Human } from '../models/Human'

export class TileSet {
  tileData:any
  currentTileRow:number = 0
  currentTileCell:number = 0
  activeTiles:Array<Array<Tile>> = new Array<Array<Tile>>()
  humans: Human[] = []
  activeLeftPos:number = 0
  activeTopPos:number= 0
  tileSize:number
  visibleRadius:number = 12

  constructor () {
    this.tileData = tileData.tileData
    this.tileSize = this.tileData.tileSize
  }
}
