'use strict'

export class Score {
    score:number
    name:string

  constructor (score, name) {
    this.score = score
    this.name = name
  }
}
