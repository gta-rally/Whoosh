'use strict'
import { Explosion } from './Explosion';
import { Slip } from './Slip';
import { Bullet } from './Bullet';

export class Car {
  id:string|undefined = undefined
  name:string|undefined = undefined
  leftPos:number = 0
  topPos:number = 0
  speed:number = 0
  width:number = 40
  length:number = 100
  weight:number = 1
  type:string|undefined = undefined
  health:number = 5
  maxHealth:number = 5
  maxSpeed:number = 0
  steerReaction:number = 0
  turnSpeed:number = 0
  friction:number = 0
  killstreak:number = 0
  killstreakMultiplier:number=1
  killstreakMultiplierInterval:any=undefined
  traction:number = 10
  carDirection:number = 0
  moveDirection:number = 0
  active:boolean = true
  phasing:boolean = true
  forward:boolean = false
  backward:boolean = false
  left:boolean = false
  right:boolean = false
  break:boolean = false
  firing:boolean = false
  explosions:Explosion[] = []
  smoke:Explosion[] = []
  bullets:Bullet[] = []
  slips:Slip[] = []
  killstreaks:string[] = []
  prompt:string = undefined
  promptInterval:any
  activeWeapon:string = ''
  noDelay:boolean=false
  godMode:boolean=false
  moveThroughWalls:boolean = false
  fireCount:number=0
  leftForce:number=0
  rightForce:number=0
  lastHitBy:string = undefined
  lastHitCounter:number=0
  smoking:any
  shield:boolean=false
  rank:number=0

  constructor () {
    
  }
  initialize(name:string, maxSpeed:number, steerReaction:number, turnSpeed:number, friction:number, width: number, length:number, weight:number, health: number, type:string, leftPos:number, topPos:number) {
    this.name = name
    this.maxSpeed = maxSpeed
    this.steerReaction = steerReaction
    this.turnSpeed = turnSpeed
    this.friction = friction
    this.width = width
    this.length = length
    this.weight = weight
    this.health = health
    this.maxHealth = health
    this.type = type
    this.leftPos = leftPos
    this.topPos = topPos
    this.killstreak = 0
    this.active = true
    this.phasing = true
  }
}
