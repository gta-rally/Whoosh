'use strict'
import tileData from '../assets/tileData.json';
import { TileSet } from '../models/TileSet';
import { Tile } from '../models/Tile';
import { Human } from '../models/Human';

export class TileService {

  spawnHumans(tileSet:TileSet,amount:number) {
    for (let i=0;i<amount;i++) {
      let spawnLocationFound = false
      do {
        let rows = tileSet.tileData.tiles.length
        let randomRow = Math.floor(Math.random() * rows)
        let cells = tileSet.tileData.tiles[randomRow].length
        let randomCell = Math.floor(Math.random() * cells)
        let cellType = tileSet.tileData.tiles[randomRow][randomCell]
        if (cellType === 'WAL') {
          let topPos = (randomRow * tileSet.tileSize) + (tileSet.tileSize / 2)
          let leftPos = (randomCell * tileSet.tileSize) + (tileSet.tileSize / 2)
          tileSet.humans.push(new Human(leftPos,topPos))
          spawnLocationFound = true
        }
      }
      while (!spawnLocationFound)
    }
  }
  updateActiveTiles(tileSet:TileSet) {
    tileSet.activeLeftPos = (tileSet.currentTileCell-tileSet.visibleRadius) * tileSet.tileSize
    tileSet.activeTopPos = (tileSet.currentTileRow-tileSet.visibleRadius) * tileSet.tileSize

    let activeRow = 0
    let activeCell = 0
    
    for (let y = tileSet.currentTileRow - tileSet.visibleRadius; y <= tileSet.currentTileRow + tileSet.visibleRadius; y++) {
        tileSet.activeTiles[activeRow] = new Array<Tile>();
      for (let x = tileSet.currentTileCell - tileSet.visibleRadius; x <= tileSet.currentTileCell + tileSet.visibleRadius; x++) {
        if (tileSet.tileData.tiles[y] !== undefined && tileSet.tileData.tiles[y][x] !== undefined) {
            tileSet.activeTiles[activeRow][activeCell] = new Tile(tileSet.tileData.tiles[y][x])
        } else {
            tileSet.activeTiles[activeRow][activeCell] = new Tile("SEA")
        }

        
        activeCell++
      }
      activeRow++
      activeCell = 0
    }
  }
  updateHumans(tileSet:TileSet) {
    for (let human of tileSet.humans){
      if (!human.alive) {
        continue
      }
      
      let endOfCrossRoadVision = (tileSet.tileSize/2)
      switch (human.moveDirection) {
        case 180:
          human.topPos+= human.movementSpeed
          if (!this.isWalkingAreaRoad(tileSet, human.topPos + endOfCrossRoadVision, human.leftPos)) {
            human.moveDirection = this.getNewMoveDirection(tileSet, human)
          }
          break
        case 0:
          human.topPos-= human.movementSpeed
          if (!this.isWalkingAreaRoad(tileSet, human.topPos - endOfCrossRoadVision, human.leftPos)) {
            human.moveDirection = this.getNewMoveDirection(tileSet, human)
          }
          break
        case 270:
          human.leftPos-= human.movementSpeed
          if (!this.isWalkingAreaRoad(tileSet, human.topPos, human.leftPos - endOfCrossRoadVision)) {
            human.moveDirection = this.getNewMoveDirection(tileSet, human)
          }
          break
        case 90:
          human.leftPos+= human.movementSpeed
          if (!this.isWalkingAreaRoad(tileSet, human.topPos, human.leftPos + endOfCrossRoadVision)) {
            human.moveDirection = this.getNewMoveDirection(tileSet, human)
          }
          break
        default:
          throw new Error("direction not defined for humans")
      }
    }
  }
  isWalkingAreaRoad(tileSet:TileSet, targetTopPos:number, targetLeftPos:number) {
    let currentTileRow = Math.floor(targetTopPos / tileSet.tileSize)
    let currentTileCell = Math.floor(targetLeftPos / tileSet.tileSize)
    let tileType = tileSet.tileData.tiles[currentTileRow][currentTileCell]

    return tileType === 'WAL' || tileType.startsWith('C')
  }
  getNewMoveDirection(tileSet:TileSet, human:Human) {
    let possibleDirections:number[] = []
    let safeCrossRoadRange = +tileSet.tileSize
    if (this.isWalkingAreaRoad(tileSet, human.topPos + safeCrossRoadRange, human.leftPos)) {
      possibleDirections.push(180)
    }
    if (this.isWalkingAreaRoad(tileSet, human.topPos - safeCrossRoadRange, human.leftPos)) {
      possibleDirections.push(0)
    }
    if (this.isWalkingAreaRoad(tileSet, human.topPos, human.leftPos + safeCrossRoadRange)) {
      possibleDirections.push(90)
    }
    if (this.isWalkingAreaRoad(tileSet, human.topPos, human.leftPos - safeCrossRoadRange)) {
      possibleDirections.push(270)
    }

    let chosenDirection = Math.floor(Math.random()*possibleDirections.length)
    return possibleDirections[chosenDirection]
  }
}
