const Tone = require('Tone');

export class SoundService {
    driveSound:any
    backwardSound:any
    breakSound:any
  
    constructor () {
    }
  
    honk() {
        var synth = new Tone.Synth().toMaster();

    //play a middle 'C' for the duration of an 8th note
        synth.triggerAttackRelease("C4", "8n");
    }

    startDrive() {
        // this.driveSound = new Tone.Noise("brown").start();
        // let autoFilter = new Tone.Tremolo({
        //     "frequency" : 19,
        //     "type" : "sine",
        //     "depth" : 0.8,
        //     "spread": 180
        // }).connect(Tone.Master);
        // //connect the noise
        // this.driveSound.connect(autoFilter);
        // autoFilter.start()
    }
    stopDrive() {
        // this.driveSound.stop()
    }
    startBackward() {
        // this.backwardSound = new Tone.Noise("brown").start();
        // let autoFilter = new Tone.AutoFilter({
        //     "frequency" : "8m",
        //     "min" : 200,
        //     "max" : 450
        // }).connect(Tone.Master);
        // //connect the noise
        // this.backwardSound.connect(autoFilter);
        // autoFilter.start()
    }
    stopBackward() {
        // this.backwardSound.stop()
    }
    startBreak() {
        // this.breakSound = new Tone.Noise("white").start();
        // let autoFilter = new Tone.AutoFilter({
        //     "frequency" : "8m",
        //     "min" : 100,
        //     "max" : 250
        // }).connect(Tone.Master);
        // //connect the noise
        // this.breakSound.connect(autoFilter);
        // autoFilter.start()
    }
    stopBreak() {
        // this.breakSound.stop()
    }

}