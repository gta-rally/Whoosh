const io = require('socket.io-client');
import { Engine } from './Engine'
import { CarService } from './CarService'
import { Car } from '../models/Car'
import { Utils } from './utils'
import { Score } from '../models/Score';

export class SocketService {
  socket:any
  carService:CarService

  constructor (carService:CarService) {
    this.carService = carService
  }

  connect(callback:any) {
    let server = 'http://' + process.env.GAME_SERVER + ':3000/';
    console.log('using server: ' + server)
    this.socket = io(server)

    this.socket.on('connect', () => {
        console.log('connected to socket io gameserver with id: ' + this.socket.id)
        callback()
    })
  }
  listen(engine:Engine) {
    this.socket.on('new-player', (data:any) => {
        let newPlayer:Car = new Car()
        newPlayer.initialize(data.name, data.maxSpeed, data.steerReaction, data.turnSpeed, data.friction, data.width, data.length, data.weight, data.health, data.type, data.leftPos, data.topPos)
        newPlayer.id = data.id
        newPlayer.godMode = data.godMode
        engine.otherPlayersList.push(newPlayer)
      })

      this.socket.on('initial-players', (players:any) => {
        engine.playersLoaded = true
        for (let index in players) {
          let data = players[index]
          let newPlayer:Car = new Car()
          newPlayer.initialize(data.name, data.maxSpeed, data.steerReaction, data.turnSpeed, data.friction, data.width, data.length, data.weight, data.health, data.type, data.leftPos, data.topPos)
          newPlayer.id = data.id
          newPlayer.maxSpeed = data.maxSpeed
          newPlayer.steerReaction = data.steerReaction
          newPlayer.turnSpeed = data.turnSpeed
          newPlayer.friction = data.friction
          newPlayer.topPos = data.topPos
          newPlayer.leftPos = data.leftPos
          newPlayer.carDirection = data.carDirection
          newPlayer.moveDirection = data.moveDirection
          newPlayer.speed = data.speed
          newPlayer.traction = data.traction
          newPlayer.health = data.health
          newPlayer.type = data.type
          newPlayer.killstreak = data.killstreak
          newPlayer.activeWeapon = data.activeWeapon
          newPlayer.phasing = data.phasing
          newPlayer.godMode = data.godMode
          newPlayer.moveThroughWalls = data.moveThroughWalls
          engine.otherPlayersList.push(newPlayer)
        }
        engine.rankOtherPlayers()
      })

      this.socket.on('remove-player', (id:string) => {
        let playerName = ''
        for (let index in engine.otherPlayersList) {
          if (engine.otherPlayersList[index].id === id) {
            playerName = engine.otherPlayersList[index].name
            engine.otherPlayersList.splice(+index, 1)
          }
        }
      })

      this.socket.on('highscore', (highscore:number, highscoreHolder:string) => {
        engine.highscore = highscore
        engine.highscoreHolder = highscoreHolder
      })

      this.socket.on('update-player-event', (data:any) => {
        for (let index in engine.otherPlayersList) {
          if (engine.otherPlayersList[index].id === data.id) {
            engine.otherPlayersList[index].leftPos = data.leftPos
            engine.otherPlayersList[index].topPos = data.topPos
            engine.otherPlayersList[index].speed = data.speed
            engine.otherPlayersList[index].traction = data.traction
            engine.otherPlayersList[index].health = data.health
            engine.otherPlayersList[index].killstreak = data.killstreak
            engine.otherPlayersList[index].carDirection = data.carDirection
            engine.otherPlayersList[index].moveDirection = data.moveDirection
            engine.otherPlayersList[index].activeWeapon = data.activeWeapon
            engine.otherPlayersList[index].phasing = data.phasing

            engine.rankOtherPlayers()
            
            switch (data.event) {
              case "startForward":
                this.carService.startForward(engine.otherPlayersList[index])
                break;
              case "startBackward":
                this.carService.startBackward(engine.otherPlayersList[index])
                break;
              case "startLeft":
                this.carService.startLeft(engine.otherPlayersList[index])
                break;
              case "startRight":
                this.carService.startRight(engine.otherPlayersList[index])
                break;
              case "startBreak":
                this.carService.startBreak(engine.otherPlayersList[index])
                break;
              case "startFiring":
                this.carService.startFiring(engine.otherPlayersList[index])
                break;
              case "endForward":
                this.carService.endForward(engine.otherPlayersList[index])
                break;
              case "endBackward":
                this.carService.endBackward(engine.otherPlayersList[index])
                break;
              case "endLeft":
                this.carService.endLeft(engine.otherPlayersList[index])
                break;
              case "endRight":
                this.carService.endRight(engine.otherPlayersList[index])
                break;
              case "endBreak":
                this.carService.endBreak(engine.otherPlayersList[index])
                break;
              case "endFiring":
                this.carService.endFiring(engine.otherPlayersList[index])
                break;
              case "bounceBack":
              case "interval":
                break;
              default:
                throw new Error ('incorrect event: ' + data.event)
            }
          }
        }
      })

      this.socket.on('bounce-back', (data:any) => {
        console.log('received bump')
        engine.mainCar.speed = data.speed
        engine.mainCar.moveDirection = data.direction
        engine.mainCar.traction = 0
        this.carService.hitBy(engine.mainCar, data.playerId, data.damage)
        this.carService.clash(engine.mainCar)
      })

      this.socket.on('receive-frag', (data:any) => {
        console.log('received frag')
        this.carService.raiseKillStreak(engine.mainCar)
      })
  }
  newPlayer(car:Car) {
    this.socket.emit("new-player", {
        "id": this.socket.id,
        "name": car.name,
        "maxSpeed": car.maxSpeed,
        "steerReaction": car.steerReaction,
        "turnSpeed": car.turnSpeed,
        "friction": car.friction,
        "leftPos": car.leftPos,
        "topPos": car.topPos,
        "length": car.length,
        "width": car.width,
        "health": car.health,
        "type": car.type,
        "weight": car.weight,
        "phasing": car.phasing,
        "godMode": car.godMode,
        "moveThroughWalls": car.moveThroughWalls
      });
  }
  updatePlayerEvent(event:string, car:Car) {
    this.socket.emit("update-player-event", {
        "id": car.id,
        "event": event,
        "leftPos": car.leftPos,
        "topPos": car.topPos,
        "speed": car.speed,
        "traction": car.traction,
        "health": car.health,
        "killstreak": car.killstreak,
        "activeWeapon": car.activeWeapon,
        "carDirection": car.carDirection,
        "moveDirection": car.moveDirection,
        "phasing": car.phasing,
        "godMode": car.godMode,
        "moveThroughWalls": car.moveThroughWalls
      });
  }
  bumpUser(id:string, speed:number, direction:number, damage:number, playerId:string) {
    this.socket.emit('bumped-user', {
        id: id,
        speed: speed,
        direction: direction,
        damage: damage,
        playerId: playerId
    })
  }
  sendFragTo(playerId:string) {
    this.socket.emit('send-frag', {
      playerId: playerId
  })
  }
  disconnect() {
    this.socket.disconnect()
  }
}