import { Car } from '../models/Car';
import { TileSet } from '../models/TileSet';
import { CarService } from './CarService'
import { Explosion } from '../models/Explosion';
import { TileService } from '../modules/TileService';
import { Bullet } from '../models/Bullet';
import { Utils } from './utils'

let attempt = 0

export class CollisionService {
    carService:CarService
    tileService:TileService = new TileService()

    constructor (carService:CarService) {
        this.carService = carService
    }

    checkTileCollissions(car: Car, tileSet: TileSet) {
        let cornerData = this.retrieveCarCornerData(car)
        let tileSize = tileSet.tileData.tileSize
        tileSet.currentTileRow = Math.floor(car.topPos / tileSize)
        tileSet.currentTileCell = Math.floor(car.leftPos / tileSize)

        if (!car.moveThroughWalls) {
            for (let y = tileSet.currentTileRow - 1; y <= tileSet.currentTileRow + 1; y++) {
                for (let x = tileSet.currentTileCell - 1; x <= tileSet.currentTileCell + 1; x++) {
                    if (x < 0 || y < 0 || y > tileSet.tileData.tiles.length || x > tileSet.tileData.tiles[y].length) {
                        continue
                    }
                    let tileType = tileSet.tileData.tiles[y][x]
                    if (tileType.startsWith('B') || tileType.startsWith('S')) {
                        let xHit = tileSet.currentTileCell - x
                        let yHit = tileSet.currentTileRow - y
                        if (xHit !== 0 && yHit !== 0) {
                            //xHit means the car bounces horizontally, yHit means the bounces vertically. When both have a value, the car needs to know how it can bounce back
                            let tileTypeX = tileSet.tileData.tiles[tileSet.currentTileRow][x]
                            let tileTypeY = tileSet.tileData.tiles[y][tileSet.currentTileCell]
                            if (tileTypeX && tileTypeY && !tileTypeX.startsWith('B') && !tileTypeX.startsWith('S') && (tileTypeY.startsWith('B') || tileTypeY.startsWith('S'))) {
                                //tile X is safe for bouncing
                                xHit = 0
                            } else if (tileTypeX && tileTypeY && !tileTypeY.startsWith('B') && !tileTypeY.startsWith('S') && (tileTypeX.startsWith('B') || tileTypeX.startsWith('S'))) {
                                //tile y is safe for bouncing
                                yHit = 0
                            }
                        }

                        if (this.carCornerOverlapsWithTile(
                            car.leftPos + cornerData.frontLeft.x,
                            car.topPos + cornerData.frontLeft.y,
                            x, y, tileSize)) {
                                this.carService.bounceBack(car, yHit, xHit, car.leftPos + cornerData.frontLeft.x,car.topPos + cornerData.frontLeft.y, this.determineDamage(car))
                            }
                        if (this.carCornerOverlapsWithTile(
                            car.leftPos + cornerData.frontRight.x,
                            car.topPos + cornerData.frontRight.y,
                            x, y, tileSize)) {
                                this.carService.bounceBack(car, yHit, xHit, car.leftPos + cornerData.frontRight.x,car.topPos + cornerData.frontRight.y, this.determineDamage(car))
                            }
                        if (this.carCornerOverlapsWithTile(
                            car.leftPos + cornerData.rearLeft.x,
                            car.topPos + cornerData.rearLeft.y,
                            x, y, tileSize)) {
                                this.carService.bounceBack(car, yHit, xHit,car.leftPos + cornerData.rearLeft.x,car.topPos + cornerData.rearLeft.y, this.determineDamage(car))
                            }
                        if (this.carCornerOverlapsWithTile(
                            car.leftPos + cornerData.rearRight.x,
                            car.topPos + cornerData.rearRight.y,
                            x, y, tileSize)) {
                                this.carService.bounceBack(car, yHit, xHit, car.leftPos + cornerData.rearRight.x,car.topPos + cornerData.rearRight.y, this.determineDamage(car))
                            }
                    }
                }
            }
        }
        
    }
    carCornerOverlapsWithTile(carX: number, carY: number, tileRow: number, tileCell: number, tileSize: number) {
        return carX > tileRow * tileSize &&
            carX < (tileRow + 1) * tileSize &&
            carY > tileCell * tileSize &&
            carY < (tileCell + 1) * tileSize
    }
    checkIfCarIsSafeToStart(mainCar: Car, otherPlayersList:Car[], socketService:any) {
        let isSafe = true
        for (let index in otherPlayersList) {
            let distanceX = mainCar.leftPos - otherPlayersList[index].leftPos
            let distanceY = mainCar.topPos - otherPlayersList[index].topPos
            let distance = Math.sqrt(Math.pow(Math.abs(distanceX), 2) + Math.pow(Math.abs(distanceY), 2))

            if (distance < mainCar.length + otherPlayersList[index].length + 20) {
                isSafe = false
            }
        }

        if (isSafe) {
            mainCar.phasing = false
            socketService.updatePlayerEvent('interval', mainCar)
        }
    }
    checkOtherPlayerCollissions(mainCar: Car, otherPlayersList:Car[], socketService:any) {
        for (let index in otherPlayersList) {
            if (otherPlayersList[index].phasing) {
                continue
            }

            let distanceX = mainCar.leftPos - otherPlayersList[index].leftPos
            let distanceY = mainCar.topPos - otherPlayersList[index].topPos
            let distance = Math.sqrt(Math.pow(Math.abs(distanceX), 2) + Math.pow(Math.abs(distanceY), 2))

            if (distance < (mainCar.length / 2) + (otherPlayersList[index].length / 2)) {
                // start car detection
                let hitDegree = Math.abs(Utils.toDegrees(Math.atan(distanceX / distanceY)))
                let mainCarDegree = Math.abs(mainCar.carDirection % 180)
                let otherCarDegree = Math.abs(otherPlayersList[index].carDirection % 180)

                let allowableDegreeOffset = 45
                let collissionDistance

                if (Math.abs(mainCarDegree - hitDegree) < allowableDegreeOffset && 
                Math.abs(otherCarDegree - hitDegree) < allowableDegreeOffset) {
                    // both cars are facing eachother in crash
                    collissionDistance = (mainCar.length / 2) + (otherPlayersList[index].length / 2)
                } else if (Math.abs(mainCarDegree - hitDegree) < allowableDegreeOffset) {
                    // main car is facing the other
                    collissionDistance = (mainCar.length / 2) + (otherPlayersList[index].width / 2)
                } else if (Math.abs(otherCarDegree - hitDegree) < allowableDegreeOffset) {
                    // other car is facing you
                    collissionDistance = (mainCar.width / 2) + (otherPlayersList[index].length / 2)
                } else {
                    // both cars are not directly facing eachother
                    collissionDistance = (mainCar.width / 2) + (otherPlayersList[index].width / 2)
                }

                if (mainCar.active && 
                    distance < collissionDistance && 
                    ((Math.abs(mainCar.speed) * mainCar.weight) > (Math.abs(otherPlayersList[index].speed) * otherPlayersList[index].weight))) {

                    if (!otherPlayersList[index].godMode && otherPlayersList[index].health > 0) {
                        otherPlayersList[index].speed = mainCar.speed
                        otherPlayersList[index].moveDirection = mainCar.moveDirection
                        otherPlayersList[index].traction = 0
    
                        let damage = mainCar.activeWeapon === 'spikes'? 3 : 1
                        socketService.bumpUser(otherPlayersList[index].id, otherPlayersList[index].speed, otherPlayersList[index].moveDirection, damage, mainCar.id)

                        mainCar.speed = otherPlayersList[index].speed
                        mainCar.moveDirection = otherPlayersList[index].moveDirection
                        this.carService.bounceBack(mainCar, distanceY / 200, distanceX / 200, mainCar.leftPos + (distanceY / 2), mainCar.topPos + (distanceX/2), 1)
                    } else {
                        mainCar.traction = 0
                        mainCar.speed = otherPlayersList[index].speed + (mainCar.speed*-1)
                        mainCar.moveDirection = otherPlayersList[index].moveDirection
                        this.carService.bounceBack(mainCar, distanceY / 20, distanceX / 20, mainCar.leftPos + (distanceY / 2), mainCar.topPos + (distanceX/2), 1)
                    }
                }
            }
        }
    }
    checkBulletEnemyCollissions(mainCar: Car, otherPlayersList:Car[], socketService:any) {
        for (let index in otherPlayersList) {
            for (let bulletIndex in mainCar.bullets) {
                if (mainCar.bullets[bulletIndex].type === 'lasergun') {
                    let laserLength = mainCar.bullets[bulletIndex].height
                    let laserMinLeftPos = mainCar.bullets[bulletIndex].leftPos + ((Math.sin(Utils.toRadians(mainCar.bullets[bulletIndex].direction)) * -(laserLength/2)))
                    let laserMaxLeftPos = mainCar.bullets[bulletIndex].leftPos + ((Math.sin(Utils.toRadians(mainCar.bullets[bulletIndex].direction)) * (laserLength/2)))

                    let laserMinTopPos = mainCar.bullets[bulletIndex].topPos - ((Math.cos(Utils.toRadians(mainCar.bullets[bulletIndex].direction)) * -(laserLength/2)))

                    if (otherPlayersList[index].leftPos > laserMinLeftPos && otherPlayersList[index].leftPos < laserMaxLeftPos ||
                        otherPlayersList[index].leftPos < laserMinLeftPos && otherPlayersList[index].leftPos > laserMaxLeftPos){
                        let playerLeftOffset = otherPlayersList[index].leftPos - laserMinLeftPos
                        let beamTopPosOffset = Math.tan(Utils.toRadians(mainCar.bullets[bulletIndex].direction - 90)) * playerLeftOffset
                        let beamTopPosAtPlayerPos = laserMinTopPos + beamTopPosOffset
                        if (Math.abs(beamTopPosAtPlayerPos - otherPlayersList[index].topPos) < otherPlayersList[index].width) {
                            otherPlayersList[index].speed = mainCar.bullets[bulletIndex].speed
                            otherPlayersList[index].moveDirection = mainCar.bullets[bulletIndex].direction
                            otherPlayersList[index].traction = 0
                            this.carService.clash(otherPlayersList[index])
        
                            socketService.bumpUser(otherPlayersList[index].id, otherPlayersList[index].speed, otherPlayersList[index].moveDirection, 0.2, mainCar.id)                    
                        }
                    }
                } else {
                    let distanceX = mainCar.bullets[bulletIndex].leftPos - otherPlayersList[index].leftPos
                    let distanceY = mainCar.bullets[bulletIndex].topPos - otherPlayersList[index].topPos
                    let distance = Math.sqrt(Math.pow(Math.abs(distanceX), 2) + Math.pow(Math.abs(distanceY), 2))
                    //shield bounce
                    if (distance < 150 && otherPlayersList[index].shield) {
                        let bullet:Bullet = mainCar.bullets[index]
                        bullet.direction += 180
                        otherPlayersList[index].bullets.push(bullet)
                        mainCar.bullets.splice(+index,1)
                        continue
                    }
                    if (distance < otherPlayersList[index].width && !otherPlayersList[index].godMode && otherPlayersList[index].active) {
                        otherPlayersList[index].speed = mainCar.bullets[bulletIndex].speed
                        otherPlayersList[index].moveDirection = mainCar.bullets[bulletIndex].direction
                        otherPlayersList[index].traction = 0
                        this.carService.clash(otherPlayersList[index])
    
                        mainCar.bullets.splice(+bulletIndex, 1)
    
                        socketService.bumpUser(otherPlayersList[index].id, otherPlayersList[index].speed, otherPlayersList[index].moveDirection, 1, mainCar.id)                    
                    }
                }
            }
        }

        // check other bullets clashing against your shield
        if (mainCar.shield) {
            for (let otherCar of otherPlayersList) {
                for (let bulletIndex in otherCar.bullets) {
                    let distanceX = mainCar.leftPos - otherCar.bullets[bulletIndex].leftPos
                    let distanceY = mainCar.topPos - otherCar.bullets[bulletIndex].topPos
                    let distance = Math.sqrt(Math.pow(Math.abs(distanceX), 2) + Math.pow(Math.abs(distanceY), 2))
                    if (distance < 150) {
                        let bullet:Bullet = otherCar.bullets[bulletIndex]
                        bullet.direction += 180
                        mainCar.bullets.push(bullet)
                        otherCar.bullets.splice(+bulletIndex, 1)
                        continue
                    }
                }
            }
        }
    }
    checkBulletTileCollissions(mainCar: Car, otherPlayersList:Car[], tileSet:TileSet,socketService:any) {
        if (mainCar.moveThroughWalls) {
            return
        }
        let tileSize = tileSet.tileData.tileSize

        //check own bullets and potentially kill others with rocket launcher
        for (let bulletIndex in mainCar.bullets) {
            let currentTileRow = Math.floor(mainCar.bullets[bulletIndex].topPos / tileSize)
            let currentTileCell = Math.floor(mainCar.bullets[bulletIndex].leftPos / tileSize)

            if (tileSet.tileData.tiles[currentTileRow] && tileSet.tileData.tiles[currentTileRow][currentTileCell]) {
                let tileType = tileSet.tileData.tiles[currentTileRow][currentTileCell]
                if (tileType && (tileType.startsWith('B') || tileType.startsWith('S'))) {
                    if (mainCar.bullets[bulletIndex].type === 'rocketlauncher') {
                        this.checkForRocketLauncherImpact(mainCar, +bulletIndex, otherPlayersList,socketService)
                    }
                    mainCar.bullets.splice(+bulletIndex, 1)
                }
            }
        }

        //check bullets from other players
        for (let index in otherPlayersList) {
            if (otherPlayersList[index].moveThroughWalls) {
                return
            }
            
            for (let bulletIndex in otherPlayersList[index].bullets) {
                let currentTileRow = Math.floor(otherPlayersList[index].bullets[bulletIndex].topPos / tileSize)
                let currentTileCell = Math.floor(otherPlayersList[index].bullets[bulletIndex].leftPos / tileSize)

                if (tileSet.tileData.tiles[currentTileRow] && tileSet.tileData.tiles[currentTileRow][currentTileCell]) {
                    let tileType = tileSet.tileData.tiles[currentTileRow][currentTileCell]
                    if (tileType && (tileType.startsWith('B') || tileType.startsWith('S'))) {
                        if (otherPlayersList[index].bullets[bulletIndex].type === 'rocketlauncher') {
                            this.carService.explodeAt(otherPlayersList[index], otherPlayersList[index].bullets[bulletIndex].leftPos, otherPlayersList[index].bullets[bulletIndex].topPos)
                        }
                        otherPlayersList[index].bullets.splice(+bulletIndex, 1)
                    }
                }
            }
        }
    }
    checkForRocketLauncherImpact(mainCar: Car, bulletIndex:number, otherPlayersList:Car[], socketService:any) {
        this.carService.explodeAt(mainCar, mainCar.bullets[bulletIndex].leftPos, mainCar.bullets[bulletIndex].topPos)
        for (let index in otherPlayersList) {
            if (otherPlayersList[index].phasing || otherPlayersList[index].health <= 0) {
                continue
            }
            let distanceX = mainCar.bullets[bulletIndex].leftPos - otherPlayersList[index].leftPos
            let distanceY = mainCar.bullets[bulletIndex].topPos - otherPlayersList[index].topPos
            let distance = Math.sqrt(Math.pow(Math.abs(distanceX), 2) + Math.pow(Math.abs(distanceY), 2))
            if (distance < 300 && !otherPlayersList[index].godMode && otherPlayersList[index].active) {
                otherPlayersList[index].traction = 0
                let hitDegree = Math.abs(Utils.toDegrees(Math.atan2(distanceX, distanceY)))
                otherPlayersList[index].moveDirection = hitDegree
                otherPlayersList[index].speed = 20

                socketService.bumpUser(otherPlayersList[index].id, otherPlayersList[index].speed, otherPlayersList[index].moveDirection, 1, mainCar.id)
            }
        }
    }
    checkCrateCollisions(car: Car, tileSet: TileSet) {
        for (let index in tileSet.tileData.crates) {
            let crate = tileSet.tileData.crates[index]
            if (crate.broken) {
                continue
            }
            let distanceX = crate.leftPos - car.leftPos
            let distanceY = crate.topPos - car.topPos
            let distance = Math.sqrt(Math.pow(Math.abs(distanceX), 2) + Math.pow(Math.abs(distanceY), 2))
            if (distance < car.width) {
                crate.broken = true
                setTimeout(()=> {
                    crate.broken = false
                },30000)
                if (crate.type === 'health') {
                    car.health = car.maxHealth
                } else {
                    car.activeWeapon = crate.type
                    car.fireCount=0
                }
            }
        }
    }
    checkTreeCollisions(car: Car, tileSet: TileSet) {
        for (let index in tileSet.tileData.trees) {
            let tree = tileSet.tileData.trees[index]
            let distanceX = car.leftPos - tree.leftPos
            let distanceY = car.topPos - tree.topPos
            let distance = Math.sqrt(Math.pow(Math.abs(distanceX), 2) + Math.pow(Math.abs(distanceY), 2))
            if (distance < car.width) {
                car.traction = 0
                car.moveDirection *= -1
                this.carService.bounceBack(car, distanceY/20, distanceX/20, car.leftPos + (distanceY / 2), car.topPos + (distanceX/2), this.determineDamage(car))
            }
        }
    }
    checkHumanCollisions(car: Car, tileSet: TileSet) {
        for (let index in tileSet.humans) {
            if (!tileSet.humans[index].alive) {
                continue
            }
            let distanceX = car.leftPos - tileSet.humans[index].leftPos
            let distanceY = car.topPos - tileSet.humans[index].topPos
            let distance = Math.sqrt(Math.pow(Math.abs(distanceX), 2) + Math.pow(Math.abs(distanceY), 2))
            if (distance < car.width/1.5) {
                if (car.speed === 0) {
                    tileSet.humans[index].moveDirection = (tileSet.humans[index].moveDirection + 180) % 360
                } else {
                    tileSet.humans[index].alive = false
                    this.carService.startPrompt(car, 'splat')
                    setTimeout(() => {
                        tileSet.humans[index].alive = true
                    },20000)
                }
            }
        }
    }
    determineDamage(car:Car) {
        if (car.godMode) {
            return 0
        } else {
            return Math.round((Math.abs(car.speed)/100) * 100) / 100
        }
    }
    checkIfEnemiesAreOutsideRange(mainCar:Car, otherPlayersList:Car[]) {
        let enemiesAreOutsideRange = true

        if (otherPlayersList.length == 0) {
            enemiesAreOutsideRange = false
        }

        for (let index in otherPlayersList) {
            let distanceX = mainCar.leftPos - otherPlayersList[index].leftPos
            let distanceY = mainCar.topPos - otherPlayersList[index].topPos
            let distance = Math.sqrt(Math.pow(Math.abs(distanceX), 2) + Math.pow(Math.abs(distanceY), 2))

            if (distance < 600) {
                enemiesAreOutsideRange = false
            }
        }
        return enemiesAreOutsideRange
    }
    retrieveCarCornerData(car:Car) {
        let cornerData = {
            distance: 0,
            degree: 0,
            frontLeft: {
              x: 0,
              y: 0
            },
            frontRight:{
              x: 0,
              y: 0
            },
            rearLeft: {
              x: 0,
              y: 0
            },
            rearRight: {
              x: 0,
              y: 0
            }
          }   

        cornerData.distance = Math.sqrt(Math.pow(car.width / 2, 2) + Math.pow(car.length / 2, 2))
        cornerData.degree = Utils.toDegrees(Math.atan(car.width / car.length))

        let topLeftDegrees = car.carDirection - cornerData.degree
        cornerData.frontLeft.x = Math.sin(Utils.toRadians(topLeftDegrees)) * cornerData.distance
        cornerData.frontLeft.y = -(Math.cos(Utils.toRadians(topLeftDegrees)) * cornerData.distance)

        let topRightDegrees = car.carDirection + cornerData.degree
        cornerData.frontRight.x = Math.sin(Utils.toRadians(topRightDegrees)) * cornerData.distance
        cornerData.frontRight.y = -(Math.cos(Utils.toRadians(topRightDegrees)) * cornerData.distance)

        let rearLeftDegrees = car.carDirection + 180 + cornerData.degree
        cornerData.rearLeft.x = Math.sin(Utils.toRadians(rearLeftDegrees)) * cornerData.distance
        cornerData.rearLeft.y = -(Math.cos(Utils.toRadians(rearLeftDegrees)) * cornerData.distance)

        let rearRightDegrees = car.carDirection + 180 - cornerData.degree
        cornerData.rearRight.x = Math.sin(Utils.toRadians(rearRightDegrees)) * cornerData.distance
        cornerData.rearRight.y = -(Math.cos(Utils.toRadians(rearRightDegrees)) * cornerData.distance)

        return cornerData
    }
}