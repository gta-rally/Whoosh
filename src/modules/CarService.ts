'use strict'
import { thisExpression } from '@babel/types';
import { Utils } from './utils';
import { SoundService } from './SoundService';
import { SocketService } from './SocketService';
import { CollisionService } from './CollisionService';
import { Explosion } from '../models/Explosion';
import { TileSet } from '../models/TileSet';
import { Car } from '../models/Car';
import { Bullet } from '../models/Bullet';
import { Slip } from '../models/Slip';

export class CarService {
  socketService:SocketService|undefined
  soundService:SoundService = new SoundService()
  collisionService:CollisionService = new CollisionService(this)
  killstreakMessages:string[] = ['BAZINGA!','SUPERB!','MULTI-KILL', 'MEGA-KILL','ULTRA-KILL','MONSTER-KILL']

  constructor () {
  }
  
  bindSocketService(socketService:SocketService,car:Car) {
    this.socketService = socketService
    car.id = socketService.socket.id
  }
  sendControlEvent(car:Car, event:string) {
    if (this.socketService !== undefined && car.active) {
      this.socketService.updatePlayerEvent(event, car)
    }
  }
  bounceBack(car:Car, yDirection:number, xDirection:number, explodeX:number, explodeY:number, damage:number) {
    car.traction /= 2
    let perfMoveDirection = 0
    car.health-= damage
    if (yDirection === 1 && xDirection === 0) {
      //bounce down
      //moving to the right
      if (car.moveDirection > 0 && car.moveDirection < 90) {
        perfMoveDirection = 90
      } else if (car.moveDirection > -270 && car.moveDirection < -360) {
        perfMoveDirection = -270
        
      //moving to the left
      } else if (car.moveDirection > 270 && car.moveDirection < 360) {
        perfMoveDirection = 270
      } else if (car.moveDirection > -90 && car.moveDirection < 0) {
        perfMoveDirection = -90
      }

      let difference = perfMoveDirection - car.moveDirection
      car.moveDirection += difference*2
      car.topPos += 10
    } else if (yDirection === -1 && xDirection === 0) {
      //bounce up
      //moving to the right
      if (car.moveDirection > 90 && car.moveDirection < 180){
        perfMoveDirection = 90
      } else if (car.moveDirection > -270 && car.moveDirection < -180) {
        perfMoveDirection = -270

      //moving to the left
      } else if (car.moveDirection > 180 && car.moveDirection < 270) {
        perfMoveDirection = 270
      } else if (car.moveDirection > -180 && car.moveDirection < -90) {
        perfMoveDirection = -90
      }

      let difference = perfMoveDirection - car.moveDirection
      car.moveDirection += difference*2
      car.topPos -= 10
    } else if (yDirection === 0 && xDirection === 1) {
      //bounce right
      //moving up
      if (car.moveDirection > -90 && car.moveDirection < 0){
        perfMoveDirection = 0
      } else if (car.moveDirection > 270 && car.moveDirection < 360) {
        perfMoveDirection = 360

      //moving down
      } else if (car.moveDirection > 180 && car.moveDirection < 270) {
        perfMoveDirection = 180
      } else if (car.moveDirection > -180 && car.moveDirection < 90) {
        perfMoveDirection = -180
      }

      let difference = perfMoveDirection - car.moveDirection
      car.moveDirection += difference*2
      car.leftPos += 10
    } else if (yDirection === 0 && xDirection === -1) {
      //bounce left
      //moving up
      if (car.moveDirection > 0 && car.moveDirection < 90) {
        perfMoveDirection = 0
      } else if (car.moveDirection > -360 && car.moveDirection < -270) {
        perfMoveDirection = -360

      //moving down
      } else if (car.moveDirection > 90 && car.moveDirection < 180) {
        perfMoveDirection = 180
      } else if (car.moveDirection > -270 && car.moveDirection < -180) {
        perfMoveDirection = -180
      }

      let difference = perfMoveDirection - car.moveDirection
      car.moveDirection += difference*2
      car.leftPos -= 10
    } else {
      //reverse bounce
      car.moveDirection += 180
      car.traction /= 2
    }
    let leftMovement = Math.sin(Utils.toRadians(car.moveDirection)) * 20
    let topMovement = Math.cos(Utils.toRadians(car.moveDirection)) * 20
    car.leftPos += leftMovement
    car.topPos -= topMovement
    
    this.clashAt(car, explodeX, explodeY)

    if (this.socketService !== undefined) {
      this.socketService.updatePlayerEvent("bounceBack", car)
    }
  }
  hitBy(car:Car, playerId:string, damage:number) {
    car.lastHitBy = playerId
    car.lastHitCounter = 100
    car.health-=damage
  }
  explodeAt(car:Car, x:number, y:number) {
    car.explosions.push(new Explosion(x,y,'boom', null));
    setTimeout(()=> {
        car.explosions.splice(0, 1)
    }, 1000)
  }
  startSmoking(car:Car, interval:number) {
    this.damage(car)
    car.smoking = setTimeout(() => {
      let nextSmoke = Math.floor(Math.random()*100 + 40)
      this.startSmoking(car, nextSmoke)
    }, interval)

  }
  damage(car:Car) {
    let leftMovement = Math.sin(Utils.toRadians(car.carDirection)) * (car.length / 4)
    let topMovement = Math.cos(Utils.toRadians(car.carDirection)) * (car.length / 4)
    let size = Math.round(Math.random()*30) + 20
    car.smoke.push(new Explosion(car.leftPos + leftMovement,car.topPos -topMovement,'smoke', size));
  }
  clashAt(car:Car, x:number, y:number) {
    car.explosions.push(new Explosion(x,y,'clash', null));
    setTimeout(()=> {
        car.explosions.splice(0, 1)
    }, 3000)
  }
  clash(car:Car) {
    car.explosions.push(new Explosion(car.leftPos,car.topPos, 'clash', null));
    setTimeout(()=> {
        car.explosions.splice(0, 1)
    }, 3000)
  }
  raiseKillStreak(car:Car) {
    car.killstreak += 100 * car.killstreakMultiplier
    this.raiseKillStreakMultiplier(car)
    let message = this.killstreakMessages[car.killstreakMultiplier] ? this.killstreakMessages[car.killstreakMultiplier] : 'BOOYAH'
    this.scream(car, message)
  }
  scream(car:Car, message:string) {
    car.killstreaks.push(message)
    setTimeout(()=> {
      car.killstreaks.splice(0, 1)
    }, 900)
  }
  update(car:Car) {
    if (car.health <= 0 && car.active) {
      if (car.lastHitBy) {
        this.socketService.sendFragTo(car.lastHitBy)
      }
      if (car.smoking !== undefined) {
        clearInterval(car.smoking)
        car.smoking = undefined
        car.smoke = []
      }
      car.active = false
      this.endForward(car)
      this.endBackward(car)
      this.endBreak(car)
      this.endFiring(car)
      this.endLeft(car)
      this.endRight(car)

      if (this.socketService) {
        this.socketService.updatePlayerEvent("interval", car)
      }
    }
    if (car.active && car.health <= 2 && car.smoking === undefined) {
      this.startSmoking(car, 200)
    } else if (car.health > 2 && car.smoking !== undefined) {
      clearInterval(car.smoking)
      car.smoking = undefined
      car.smoke = []
    }
    if (car.smoking !== undefined && car.smoke.length > 20) {
      car.smoke.splice(0,1)
    }
    for (let smoke of car.smoke) {
      smoke.size++
      if (smoke.lifespan < 100) {
        smoke.lifespan++
      }
    }
    if (car.lastHitBy) {
      car.lastHitCounter--
      if (car.lastHitCounter <= 0) {
        car.lastHitBy = undefined
      }
    }
    this.adjustSpeedAndDirection(car)
    this.adjustPosition(car)
  }
  startPrompt(car:Car, text:string) {
    if (car.promptInterval !== undefined) {
      clearInterval(car.promptInterval)
    }
    car.killstreak += Math.pow(2, car.killstreakMultiplier)
    this.raiseKillStreakMultiplier(car)
    car.prompt = ''
    let char = 0
    car.promptInterval = setInterval(() => {
      if (char < text.length) {
        char++
        car.prompt = text.substr(0, char)
      } else {
        clearInterval(car.promptInterval)
        car.promptInterval = undefined
        setTimeout(() => {
          car.prompt = undefined
        }, 5000)
      }
    }, 200)
  }
  raiseKillStreakMultiplier(car:Car) {
    if (car.killstreakMultiplierInterval) {
      clearInterval(car.killstreakMultiplierInterval)
    }
    car.killstreakMultiplier++
    car.killstreakMultiplierInterval = setTimeout(() => {
      car.killstreakMultiplier = 1
      clearInterval(car.killstreakMultiplierInterval)
    },2000)

  }

  adjustSpeedAndDirection(car:Car) {
    if (car.forward) this.moveForward(car);
    if (car.backward) this.moveBackward(car);
    if (car.left) this.steerLeft(car);
    if (car.right) this.steerRight(car);
    if (car.break) this.doBreak(car);
    if (car.firing && car.activeWeapon !== '') this.fireBullet(car);
    
    if (!car.forward || !car.backward) {
      let directionDifference = car.carDirection - car.moveDirection
      car.moveDirection += (directionDifference / 10) * car.traction
      car.moveDirection = car.moveDirection % 360
      car.carDirection = car.carDirection % 360
    }

    for (let bulletIndex in car.bullets) {
      if (car.bullets[bulletIndex].type === 'lasergun') {
        let bullet = car.bullets[bulletIndex]
        car.bullets[bulletIndex].direction = car.carDirection
        car.bullets[bulletIndex].height = this.determineLaserHeight(car.leftPos, car.topPos, car.carDirection, 10000)

        let leftPosition = (Math.sin(Utils.toRadians(car.carDirection)) * (bullet.height/2))
        let topPosition = (Math.cos(Utils.toRadians(car.carDirection)) * (bullet.height/2))

        car.bullets[bulletIndex].leftPos = car.leftPos + leftPosition
        car.bullets[bulletIndex].topPos = car.topPos - topPosition
      } else {
        let leftMovement = Math.sin(Utils.toRadians(car.bullets[bulletIndex].direction)) * car.bullets[bulletIndex].speed
        let topMovement = Math.cos(Utils.toRadians(car.bullets[bulletIndex].direction)) * car.bullets[bulletIndex].speed
  
        car.bullets[bulletIndex].leftPos += leftMovement
        car.bullets[bulletIndex].topPos -= topMovement
      }

      car.bullets[bulletIndex].counter++
      if (car.bullets[bulletIndex].counter >= car.bullets[bulletIndex].lifespan) {
        car.bullets.splice(+bulletIndex,1)
      }
    }

    car.speed *= car.friction
    if (car.speed < .1 && car.speed > -.1) {
      car.speed = 0
      car.traction = 10
    }

  }
  adjustPosition(car:Car) {
    let leftMovement = Math.sin(Utils.toRadians(car.moveDirection)) * car.speed
    let topMovement = Math.cos(Utils.toRadians(car.moveDirection)) * car.speed

    car.leftPos += leftMovement
    car.topPos -= topMovement
  }
  moveForward(car:Car) {
    if (car.traction < 10) {
      if (car.speed < 5 && car.speed > -5) {
        car.traction = 10
      }
    } else {
      car.traction = 10
    }
    if (car.speed < car.maxSpeed && car.moveDirection === car.carDirection) {
      let damageSlowness = car.health <= 2? 0.5 : 1
      car.speed+= (0.1 *car.traction) * damageSlowness
    }
  }
  moveBackward(car:Car) {
    if (car.traction < 10) {
      if (car.speed < 5 && car.speed > -5) {
        car.traction = 10
      }
    } else {
      car.traction = 10
    }
    if (car.speed > -car.maxSpeed && (car.moveDirection === car.carDirection)) {
      car.speed -= 0.05 * car.traction
    }
  }
  doBreak(car:Car) {
    car.traction = 0
    this.addSlip(car)
    if (car.moveDirection === car.carDirection) {
      if (car.speed >= 1) {
        car.speed--;
      } else if (car.speed <= -1) {
        car.speed++;
      } else {
        car.speed = 0;
      }
    }
  }
  addSlip(car:Car) {
    car.slips.push(new Slip(car.leftPos,car.topPos, car.carDirection));
    setTimeout(()=> {
        car.slips.splice(0, 1)
    }, 4000)
  }
  fireBullet(car:Car) {
    let delay
    let direction
    let speed
    let lifespan
    let height
    let width
    switch (car.activeWeapon) {
      case "uzi":
        delay = 10
        direction = car.carDirection
        speed = car.speed + 20
        lifespan = 40
        height = 12
        width = 8
        break
      case "mines":
        delay = 10
        direction = 0
        speed = 0
        lifespan = 100
        height = 16
        width = 16
        break
      case "flamethrower":
        delay = 1
        direction = car.carDirection
        speed = car.speed + 8
        lifespan = 20
        height = 20
        width = 20
        break
      case "rocketlauncher":
        delay = 50
        direction = car.carDirection
        speed = car.speed + 10
        lifespan = 200
        height = 16
        width = 8
        break
      case "spikes":
        delay = 0
        direction = 0
        speed = 0
        lifespan = 0
        break
      case "shotgun":
        delay = 30
        direction = car.carDirection
        speed = car.speed + 20
        lifespan = 25
        height = 12
        width = 8
        break
      case "lasergun":
        delay = 100
        direction = car.carDirection
        speed = 0
        lifespan = 40
        width = 12
        break
      case "shield":
        car.shield=true
        return
      default:
        throw new Error('active weapon ' + car.activeWeapon + ' not found')
    }
    if (car.noDelay) {
      delay = 1
    }
    
    if (car.fireCount % delay === 0) {
      if (car.activeWeapon === 'shotgun') {
        for (let i = direction-20;i<direction+20;i+=2) {
          let bullet = new Bullet(car.activeWeapon, car.leftPos, car.topPos, i, speed, lifespan, width, height)
          car.bullets.push(bullet)
        }
        
      } else if (car.activeWeapon === 'uzi') {
        let leftMovement = Math.sin(Utils.toRadians(direction+90)) * (car.width/3.3)
        let topMovement = Math.cos(Utils.toRadians(direction+90)) * (car.width/3.3)
        car.bullets.push(new Bullet(car.activeWeapon, car.leftPos + leftMovement, car.topPos - topMovement, direction, speed, lifespan, width, height))

        leftMovement = Math.sin(Utils.toRadians(direction-90)) * (car.width/3.3)
        topMovement = Math.cos(Utils.toRadians(direction-90)) * (car.width/3.3)
        car.bullets.push(new Bullet(car.activeWeapon, car.leftPos + leftMovement, car.topPos - topMovement, direction, speed, lifespan, width, height))

      } else if (car.activeWeapon === 'mines') {
        let leftPosition = Math.sin(Utils.toRadians(car.carDirection)) * (car.length/2)
        let topPosition = Math.cos(Utils.toRadians(car.carDirection)) * (car.length/2)
        let bullet = new Bullet(car.activeWeapon, car.leftPos - leftPosition, car.topPos + topPosition, direction, speed, lifespan, width, height)
        car.bullets.push(bullet)

      } else if (car.activeWeapon === 'lasergun') {
        height = this.determineLaserHeight(car.leftPos, car.topPos, direction, 10000)

        let leftPosition = (Math.sin(Utils.toRadians(direction)) * (height/2))
        let topPosition = (Math.cos(Utils.toRadians(direction)) * (height/2))
        let bullet = new Bullet(car.activeWeapon, car.leftPos + leftPosition, car.topPos - topPosition, direction, speed, lifespan, width, height)
        car.bullets.push(bullet)

      } else {
        let leftPosition = Math.sin(Utils.toRadians(direction)) * (car.length/2)
        let topPosition = Math.cos(Utils.toRadians(direction)) * (car.length/2)
        let bullet = new Bullet(car.activeWeapon, car.leftPos + leftPosition, car.topPos - topPosition, direction, speed, lifespan, width, height)
        car.bullets.push(bullet)
      }
    }
    car.fireCount++
  }
  steerLeft(car:Car) {
    if (car.leftForce < 1) {
      car.leftForce+=car.steerReaction
    }
    if (car.speed >= 0) {
      car.carDirection-=this.getTurnSpeed(car)*car.leftForce;
    } else {
      car.carDirection+=this.getTurnSpeed(car)*car.leftForce;
    }
  }
  steerRight(car:Car) {
    if (car.rightForce < 1) {
      car.rightForce+=car.steerReaction
    }
    if (car.speed >= 0) {
      car.carDirection+=this.getTurnSpeed(car)*car.rightForce;
    } else {
      car.carDirection-=this.getTurnSpeed(car)*car.rightForce;;
    }
  }
  getTurnSpeed(car:Car) {
    if (car.break) {
      return (car.turnSpeed * (Math.abs(car.speed * 2) / 10))
    } else {
      let idealspeed = car.maxSpeed / 2
      if (Math.abs(car.speed) < idealspeed) {
        return car.turnSpeed * Math.min((Math.abs(car.speed) / 10), 1)
      } else {
        let overSpeed = (Math.abs(car.speed) - idealspeed) * 2
        return car.turnSpeed * Math.min(((Math.abs(car.speed) - overSpeed) / 10), 1)
      }
    }
    
  }
  startBreak(car:Car) {
    if (car.active) {
      if (car.break === false) {
        this.soundService.startBreak()
      }
      car.break = true
    }
  }
  endBreak(car:Car) {
    car.break = false
    this.soundService.stopBreak()
  }
  startLeft(car:Car) {
    if (car.active) {
      car.left = true
    }
  }
  endLeft(car:Car) {
    car.left = false
    car.leftForce = 0
  }
  startRight(car:Car) {
    if (car.active) {
      car.right = true
    }
  }
  endRight(car:Car) {
    car.right = false
    car.rightForce = 0
  }
  startForward(car:Car) {
    if (car.active) {
      if (car.forward === false) {
        this.soundService.startDrive()
      }
      car.forward = true
    }
  }
  endForward(car:Car) {
    car.forward = false
    this.soundService.stopDrive()
  }
  startBackward(car:Car) {
    if (car.active) {
      if (car.backward === false) {
        this.soundService.startBackward()
      }
      car.backward = true
    }
  }
  endBackward(car:Car) {
    car.backward = false
    this.soundService.stopBackward()
  }
  startFiring(car:Car) {
    if (car.active) {
      car.firing = true
    }
  }
  endFiring(car:Car) {
    car.firing = false
    car.fireCount = 0
    if (car.activeWeapon==='shield') {
      car.shield = false
    }
  }
  honk(car:Car) {
    if (car.active) {
      this.soundService.honk()
    }
  }

  /*method should not be here, haven't figured a better way to solve this yet */
  determineLaserHeight(leftPos:number, topPos:number, direction:number, sizeToCheck:number) { 
    let tileSet = new TileSet()
    let tileSize = tileSet.tileData.tileSize

    let newLeftPos
    let newTopPos

    for (let i=0;i<sizeToCheck;i+=10) {
        let leftMovement = Math.sin(Utils.toRadians(direction)) * i
        let topMovement = Math.cos(Utils.toRadians(direction)) * i
        newLeftPos = leftPos + leftMovement
        newTopPos = topPos - topMovement

        let currentTileRow = Math.floor(newTopPos / tileSize)
        let currentTileCell = Math.floor(newLeftPos / tileSize)

        if (tileSet.tileData.tiles[currentTileRow] && tileSet.tileData.tiles[currentTileRow][currentTileCell]) {
            let tileType = tileSet.tileData.tiles[currentTileRow][currentTileCell]
            if (tileType && (tileType.startsWith('B') || tileType.startsWith('S'))) {
              return Math.sqrt(Math.pow(Math.abs(leftPos - newLeftPos), 2) + Math.pow(Math.abs(topPos - newTopPos), 2))
            }
        }
    }
    return sizeToCheck
}
}
