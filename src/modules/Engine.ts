import { TileSet } from '../models/TileSet';
import { CollisionService } from './CollisionService'
import { SocketService } from './SocketService'
import { CarService } from './CarService'
import { TileService } from './TileService'
import { Car } from '../models/Car'
import carProperties from '../assets/carProperties.json';


enum Cheats {
  ULTRASPEED = 'ULTRASPEED',
  NODELAY = 'NODELAY',
  STARTWITHUZI = 'STARTWITHUZI',
  STARTWITHSHIELD = 'STARTWITHSHIELD',
  GODMODE = 'GODMODE',
  NOCLIP = 'NOCLIP',
  STARTDAMAGED = 'STARTDAMAGED',
  TATU = 'TATU',
  BUSJEKOMEZO = 'BUSJEKOMEZO',
  STARTWITHHIGHSCORE = 'STARTWITHHIGHSCORE'
}

export class Engine {
  mainCar: Car = new Car()
  tileSet: TileSet = new TileSet()
  tileService: TileService
  socketService: SocketService
  carService: CarService
  fps: number = 60
  scale: number = 1
  playerName: string = ''
  carProperties:any
  otherPlayersList:Car[] = []
  collisionService: CollisionService
  allEnemiesAreOutsideRange=false
  playersLoaded:boolean=false
  highscore:number = 0
  highscoreHolder:string = ''
  countdown:number = 3
  unlockedCheats:String[] = []

  constructor() {
    this.carProperties = carProperties
    this.carService = new CarService()
    this.socketService = new SocketService(this.carService)
    this.collisionService = new CollisionService(this.carService)
    this.tileService = new TileService()
  }
  play(playerName: string, cheatCode: string, selectedCarProperty: any) {
    this.playerName = playerName
    let startX = this.tileSet.tileData.playerStartPos.x
    let startY = this.tileSet.tileData.playerStartPos.y
    this.mainCar.initialize(
      playerName, 
      selectedCarProperty.maxSpeed, 
      selectedCarProperty.steerReaction, 
      selectedCarProperty.turnSpeed, 
      selectedCarProperty.friction, 
      selectedCarProperty.width, 
      selectedCarProperty.length, 
      selectedCarProperty.weight,
      selectedCarProperty.health, 
      selectedCarProperty.name + '-' + selectedCarProperty.selected, startX, startY)

    this.activateCheatCode(cheatCode)

    this.socketService.connect(() => {
      this.socketService.listen(this)
      this.carService.bindSocketService(this.socketService,this.mainCar)
      this.socketService.newPlayer(this.mainCar)
      let startTime = (new Date()).getTime()
      // this.tileService.spawnHumans(this.tileSet, 5)

      setInterval(() => {
        this.collisionService.checkTileCollissions(this.mainCar, this.tileSet)
        if (this.mainCar.phasing && this.playersLoaded) {
          if ((new Date()).getTime() - startTime > ((3 - this.countdown) * 1000) && this.countdown > 0) {
            this.carService.scream(this.mainCar, this.countdown.toString())
            this.countdown--
          }
          if ((new Date()).getTime() - startTime > 3000) {
            if (this.mainCar.killstreaks.length === 0) {
              this.carService.scream(this.mainCar, 'GO!')
            }
            
            this.collisionService.checkIfCarIsSafeToStart(this.mainCar, this.otherPlayersList, this.socketService)
          }
        } else {
          this.collisionService.checkOtherPlayerCollissions(this.mainCar, this.otherPlayersList, this.socketService)
          this.collisionService.checkBulletEnemyCollissions(this.mainCar, this.otherPlayersList, this.socketService)
          this.collisionService.checkBulletTileCollissions(this.mainCar, this.otherPlayersList, this.tileSet, this.socketService)
          this.collisionService.checkCrateCollisions(this.mainCar, this.tileSet)
          this.collisionService.checkTreeCollisions(this.mainCar, this.tileSet)
          // this.collisionService.checkHumanCollisions(this.mainCar, this.tileSet)
        }
        
        this.allEnemiesAreOutsideRange = this.collisionService.checkIfEnemiesAreOutsideRange(this.mainCar, this.otherPlayersList)

        for (let otherCar of this.otherPlayersList) {
          this.carService.update(otherCar)
        }

        this.carService.update(this.mainCar)
        this.tileService.updateActiveTiles(this.tileSet)
        // this.tileService.updateHumans(this.tileSet)
        this.updateScale()
      }, 1000 / this.fps)

      setInterval(() => {
        this.socketService.updatePlayerEvent("interval", this.mainCar)
      }, 1000)
    })
  }
  rankOtherPlayers() {
    if (this.otherPlayersList.length > 1) {
      this.otherPlayersList.sort((c1, c2) => (c1.killstreak > c2.killstreak ? -1 : 1))
    }
    for (let i =0 ; i< this.otherPlayersList.length;i++) {
      if (this.otherPlayersList[i].killstreak > this.mainCar.killstreak) {
        this.otherPlayersList[i].rank = i
      } else {
        this.otherPlayersList[i].rank = i + 1
      }
    }

    this.mainCar.rank = 0
    for (let car of this.otherPlayersList) {
      if (car.killstreak > this.mainCar.killstreak) {
        this.mainCar.rank++
      }
    }
  }
  unlockCheats() {
    console.log('unlocking cheats')
    this.unlockedCheats = []
    let cheatsToUnlock:number = Math.floor(this.mainCar.killstreak / 1000)
    let availableCheatsTotal = Object.keys(Cheats).length

    if (cheatsToUnlock > availableCheatsTotal) {
      cheatsToUnlock = availableCheatsTotal
    }

    let listOfCheats = []
    for (let i=0;i<availableCheatsTotal;i++) {
      listOfCheats.push(Object.keys(Cheats)[i])
    }
    
    for (let i=0;i<cheatsToUnlock;i++) {
      let availableCheats = listOfCheats.length
      let randomCheat:number = Math.floor(Math.random() * availableCheats)
      this.unlockedCheats.push(listOfCheats[randomCheat])
      listOfCheats.splice(randomCheat, 1)
    }
  }
  activateCheatCode(cheatCode:string) {
    switch (cheatCode) {
      case Cheats.ULTRASPEED:
        this.mainCar.maxSpeed += 20
        this.mainCar.friction += 0.02
        break
      case Cheats.NODELAY:
        this.mainCar.noDelay = true
        break
      case Cheats.STARTWITHUZI:
        this.mainCar.activeWeapon = 'uzi'
        break
      case Cheats.STARTWITHSHIELD:
        this.mainCar.activeWeapon = 'shield'
        break
      case Cheats.GODMODE:
        this.mainCar.godMode = true
        break
      case Cheats.NOCLIP:
        this.mainCar.moveThroughWalls = true
        break
      case Cheats.STARTDAMAGED:
        this.mainCar.health = 2
        break
      case Cheats.TATU:
        let police = carProperties.cars[3]
        this.mainCar.maxSpeed = police.maxSpeed
        this.mainCar.steerReaction = police.steerReaction
        this.mainCar.turnSpeed = police.turnSpeed
        this.mainCar.friction = police.friction
        this.mainCar.width = police.width
        this.mainCar.length = police.length
        this.mainCar.weight = police.weight
        this.mainCar.health = police.health
        this.mainCar.type = 'car-police-default'
        break
      case Cheats.BUSJEKOMEZO:
        let truck = carProperties.cars[4]
        this.mainCar.maxSpeed = truck.maxSpeed
        this.mainCar.steerReaction = truck.steerReaction
        this.mainCar.turnSpeed = truck.turnSpeed
        this.mainCar.friction = truck.friction
        this.mainCar.width = truck.width
        this.mainCar.length = truck.length
        this.mainCar.weight = truck.weight
        this.mainCar.health = truck.health
        this.mainCar.maxHealth = truck.health
        this.mainCar.type = 'truck-1-yellow'
        break
      case Cheats.STARTWITHHIGHSCORE:
        this.mainCar.killstreak = 5000
        break
      default:
        break
    }
  }
  updateScale() {
    this.scale = Math.max(1 - ((Math.abs(this.mainCar.speed)) / 40), 0.7)
  }
  toRadians (angle:number) {
    return angle * (Math.PI / 180)
  }
}
