# Whoosh - the game

You are a GTA style car cruising through the ghetto, until you find a squeaking guinea pig in the side of the road. What is it doing here, but more importantly. Why does it think it's allowed on MY ROAD?! Among others, your goal will be to find this guinea pig and git rid of it for all. Make the road yours again! Ow and you also might find other drivers along the road, who also think they are allowed here. I don't think I need to mention what to do with them...

### Roadmap
1) First we will implement cruising through the ghetto. Just some core gameplay stuff:
- ~~Adding visuals to the car~~
- ~~Design a map that you drive through~~
- ~~Visuals for road~~
- ~~Visuals for top of buildings (that you crash in to)~~
- ~~Adding collision points in the ghetto, so that you can actually crash~~
- ~~Adding crash mechanics (something like 3 hits and you're kaboom!)
- ~~some minor improvements on the driving mechanics~~
- ~~When driving faster, you should zoom out, just like the old GTA games did~~
- ~~Add whacky sounds~~
- ~~getting it live~~

2) Next we will add some other stuff to hit or kill along the way. Mainly stuff to keep you engaged
- ~~new map visuals~~
- ~~tiles 120x120~~
- ~~dynamic map rendering (only render tiles close to you)~~
- ~~add extra row / column with water so you cant drive off map~~
- ~~add water asset (not grey)~~
- ~~water background~~
- ~~fix bug visible area = other car~~
- add explosion animation (Danny)
- ~~add collision animation (Danny)~~
- ~~trees with collision~~
- ~~dead car~~
- ~~login UI~~
- ~~active weapon UI~~
- ~~health UI~~
- ~~playerlist UI~~
- ~~car with damage~~
- ~~fix killstreak bugs, send hitEvent incl playerid -> other player receives damage -> if damage drops to 0, send event to player id for killstreak++~~
- ~~slipsporen~~
- ~~map spawnpoints: boxes (Danny)~~
- ~~animation: killstreak etc~~
- ~~sliding (Fin)~~
- ~~selecting car~~
- ~~killstreak board~~
- message board (with occasionally random shit)
- ~~crates with rewards~~
- ~~Multi player option (log in with your person)~~
- ~~walking AI supported things on the sidewalk that you can use to paint your tires a new color~~
- driving cars on the road that you can bump into which might chase you
- ~~cheats~~